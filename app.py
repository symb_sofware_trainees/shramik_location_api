from flask import Flask, request, jsonify
import pymongo
import requests
import os
from flask_cors import CORS,cross_origin
from flask import Flask, request, jsonify
import pymongo
import requests
import os
from flask_cors import  cross_origin
from twilio.rest import Client
from datetime import datetime
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity,unset_jwt_cookies,create_access_token, create_refresh_token, set_access_cookies, set_refresh_cookies

from pymongo import MongoClient
from datetime import timedelta
app = Flask(__name__)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)
CORS(app)

app.config['JWT_SECRET_KEY'] = 'ahhahahasfdgfhjhngbfvddfn134566gfasdfn54'  # Replace with your own secret key
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(hours=1)


# Connect to MongoDB
client = pymongo.MongoClient('mongodb+srv://Yashee:1234@student.dd1pfaw.mongodb.net/?retryWrites=true&w=majority')
db = client['Shramik']
worker_table = db['Worker']
user_table = db['User']
booking_table=db['Booking']
review_table=db['Review']


@app.route('/signup', methods=['POST'])
@cross_origin()
def signup():
    # Get user data from request body
    username = request.json['username']
    password = request.json['password']
    mobile_number = request.json['mobile_number']
    address = request.json['address']

    # Hash the password
    hashed_password = bcrypt.generate_password_hash(password).decode('utf-8')

    # Check if the username already exists
    if user_table.find_one({'username': username}):
        return jsonify({'message': 'Username already exists'}), 400
    last_user = user_table.find().sort('user_id', -1).limit(1)
    last_id = last_user[0]['user_id'] if last_user else 0

    # Create a new user document
    user = {
        'user_id':last_id+1,
        'username': username,
        'password': hashed_password,
        'mobile_number': mobile_number,
        'address': address
    }

    # Insert the user document into the users collection
    user_table.insert_one(user)

    return jsonify({'message': 'Signup successful'})

@app.route('/login', methods=['POST'])
@cross_origin()
def login():
    # Get user credentials from request body
    username = request.json['username']
    password = request.json['password']

    # Find the user in the database
    user = user_table.find_one({'username': username})
    if not user or not bcrypt.check_password_hash(user['password'], password):
        return jsonify({'message': 'Invalid username or password'}), 401
    
    # Generate access token with username as payload
    access_token = create_access_token(identity=user['username'])
    refresh_token = create_refresh_token(identity=user['username'])
    response = jsonify({'message': 'Login successful'})
    # Set JWT cookies in the responnse
    set_access_cookies(response, access_token)
    set_refresh_cookies(response, refresh_token)

    return jsonify({'access_token': access_token,'id':user['user_id'],'userName':user['username'],'mobile_number':user['mobile_number']})

@app.route('/logout', methods=['POST'])
@cross_origin()
@jwt_required()
def logout():
    resp = jsonify({'msg': 'Logged out successfully'})
    unset_jwt_cookies(resp)
    return resp

@app.route('/protected', methods=['GET'])
@jwt_required()
def protected():
    # Access the identity of the current user
    current_user = get_jwt_identity()

    return jsonify({'message': f'Protected endpoint accessed by {current_user}'})

@app.route('/worker_reviews/<int:worker_id>', methods=['GET'])
@cross_origin()
@jwt_required()
def get_worker_reviews(worker_id):
    # Find all reviews for the given worker_id
    reviews = review_table.find({'worker_id': worker_id})
    worker = worker_table.find_one({'_id': worker_id})
    
    # Create a list to hold the review data
    review_list = []
    for review in reviews:
        # Extract the necessary fields from each review
        review_data = {
            'review': review['review'],
            'rating': review['rating'],
            'worker_id': review['worker_id'],
            'user_id': review['user_id']
        }
        review_list.append(review_data)

    # Return the list of reviews as JSON response
    return jsonify({'reviews': review_list,'worker':worker})

@app.route('/review', methods=['POST'])
@cross_origin()
@jwt_required()
def submit_review():
    # Get data from request body
    review = request.json['review']
    rating = request.json['rating']
    worker_id = request.json['worker_id']
    user_id = request.json['user_id']
    booking = booking_table.find_one({'user_id': user_id, 'worker_id': worker_id})
    if not booking:
        return jsonify({'message': 'No matching booking found. Cannot submit rating.'}), 400

    worker = worker_table.find_one({'_id': worker_id})
    # Add a row to the rating_table
    rating_row = {
        'review': review,
        'rating': rating,
        'worker_id': worker_id,
        'user_id': user_id
    }
    review_table.insert_one(rating_row)

    # Return success response
    return jsonify({'message': 'Review Rating submitted successfully!','review':rating_row,'worker':worker})

@app.route('/calculate_distance/<int:id>', methods=['POST'])
@cross_origin()
@jwt_required()
def calculate_distance(id):
    # Get user's location from HTML5 Geolocation API
    # user_location = request.json['user_location']
    # ip_address = request.remote_addr
    # ip_address='122.163.134.49'
    url = f'http://api.ipapi.com/api/check?access_key=40904e78dc2c3376fbf83cc3f5129638'

    response = requests.get(url)
    
    data = response.json()
    print(data)
    latitude = data['latitude']
    longitude = data['longitude']

    user_location = f"{latitude},{longitude}"
    
    # Get worker's location from MongoDB
    worker = worker_table.find_one({'_id': id})
    location=str(worker['latitude'])+","+(str(worker['longitude']))
    # Build URL for distancematrix.ai API call
    url = 'https://api.distancematrix.ai/maps/api/distancematrix/json?origins=' + location + '&destinations=' + user_location + '&key=oRrwgMt6XfRfu37DgulanIdBJnXvl'
    
    # Make API call to get distance and time information
    response = requests.get(url)
    data = response.json()
    # print(data)
    # Parse distance and time information from API response
    distance = data['rows'][0]['elements'][0]['distance']['text']
    time = data['rows'][0]['elements'][0]['duration']['value']
    
    # Return distance and estimated travel time
    return jsonify({
        'distance': distance,
        'time': str(time*4//60)+"min"
    })

@app.route('/fetch_nearest_worker', methods=['POST'])
@cross_origin()
@jwt_required()
def fetch_worker():
    
    # Get user's location from request body
    # user_location = request.json['user_location']
    # ip_address = request.remote_addr
    # ip_address='122.163.134.49'
    url = f'http://api.ipapi.com/api/check?access_key=40904e78dc2c3376fbf83cc3f5129638'
    response = requests.get(url)
    data = response.json()
    # data = requests.get(url)
    print(data)
    latitude = data['latitude']
    longitude = data['longitude']
    user_location = f"{latitude},{longitude}"
    
    # Fetch all worker details from MongoDB
    workers = worker_table.find()
    
    # Define dictionary to hold worker details and distance/time information
    result = {}
    
    # Iterate through all workers
    for worker in workers:
        # Build URL for distancematrix.ai API call
        location=str(worker['latitude'])+","+(str(worker['longitude']))
        # Build URL for distancematrix.ai API call
        url = 'https://api.distancematrix.ai/maps/api/distancematrix/json?origins=' + user_location + '&destinations=' + location + '&key=oRrwgMt6XfRfu37DgulanIdBJnXvl'
    
        # Make API call to get distance and time information
        response = requests.get(url)
        data = response.json()
        print(data)
        # Parse distance and time information from API response
        distance = data['rows'][0]['elements'][0]['distance']['text']
        time = data['rows'][0]['elements'][0]['duration']['value']
        
        # Add worker details and distance/time information to result dictionary
        result[worker['_id']] = {
            'name':worker['userName'],
            'distance': distance,
            'time': str(time*4//60)+"min"
        }
    
    # Return result dictionary as JSON response
    sorted_workers = sorted(result.values(), key=lambda x: x['distance'])
    return jsonify(sorted_workers[0])


account_sid = 'AC061c226fcf4b4a274d4ae493e4cd12a8'
auth_token = 'bba38c53e1f491f4aedbd72b1958697f'
twilio_number = '+16203018945'
tclient = Client(account_sid, auth_token)

@app.route('/book_worker', methods=['POST'])
@cross_origin()
@jwt_required()
def book_worker():
# Get data from request body
    user_mobile = request.json['user_mobile']
    worker_id = request.json['worker_id']
    user_id = request.json['user_id']
    # Find worker with given ID in MongoDB
    worker = worker_table.find_one({'_id': worker_id})
    print(worker)
    # Find user with given mobile number in MongoDB
    user = user_table.find_one({'user_id':user_id})
    # pi
    # Get user's name from MongoDB
    user_name = user['username']

    # Send message to worker to notify them that they have been booked
    worker_message = f'Hi {worker["userName"]}, you have been booked by {user_name}. Please contact them at {user_mobile} for further details.'
    send_message(worker['mobNumber'], worker_message)

    # Send message to user to confirm booking
    user_message = f'Hi {user_name}, you have successfully booked {worker["userName"]}. They will contact you at {user_mobile} for further details.'
    send_message(user_mobile, user_message)

    booking_row = {
        'booked_at': datetime.now(),
        'serviced_at': None,
        'user_id': user_id,
        'worker_id': worker_id,
        'price': worker["rate"],
        'worker_name':worker["userName"],
        'status': 'booked'
    }
    booking_table.insert_one(booking_row)
    # Return success response
    return jsonify({'message': 'Booking successful!'})
def send_message(to, body):
    message = tclient.messages.create(
        body=body,
        from_=twilio_number,
        to=to
    )
    return message

@app.route('/user_bookings/<int:user_id>', methods=['GET'])
@cross_origin()
@jwt_required()
def get_user_bookings(user_id):
    # Find all bookings of the user in the booking_table collection
    user_bookings = booking_table.find({'user_id': user_id})
    
    # Create a list to store the booking details
    bookings = []

    # Iterate over the bookings and extract relevant information
    for booking in user_bookings:
        # print(booking)
        # Extract necessary fields from the booking document
        booking_data = {
            'booked_at': booking['booked_at'],
            'serviced_at': booking['serviced_at'],
            'worker_id': booking['worker_id'],
            'price': booking['price'],
            'worker_name':booking["worker_name"],
            'status': booking['status']
        }
        bookings.append(booking_data)
    if len(bookings) == 0:
        return jsonify({'message': 'No bookings found for the user.'}), 404

    # print(bookings)
    # Return the list of bookings as a JSON response
    return jsonify({'bookings': bookings})

if __name__ == '__main__':
    app.run(debug=True)